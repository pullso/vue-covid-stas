import Vue from 'vue'
import Router from 'vue-router'
import Countries from '@/components/Countries'
import Main from '../components/Main'
import Country from '../components/Country'
import store from '../store/index'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/countries',
      name: 'Countries',
      component: Countries
    },
    {
      path: '/country/:queryName',
      name: 'Country',
      component: Country,
      props: true,
      beforeEnter (to, from, next) {
        let queryName = to.params.queryName
        store.dispatch('newCountryName', queryName)
        next()
      }
    }
  ]
})
