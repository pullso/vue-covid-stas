import axios from 'axios'
import Country from './countryPrototype'

export default {
  state: {
    currCountryName: null,
    countries: [],
    topCountries: []
  },
  mutations: {
    newCountry: (s, payload) => { s.countries.push(payload) },
    newCountryName: (s, payload) => { s.currCountryName = payload },
    newTopCountry: (s, payload) => { s.topCountries = payload }
  },
  actions: {
    newCountry ({commit, getters}, payload) {
      commit('clearError')
      commit('setLoading', true)

      console.log(`new country is ${payload}`)

      axios.get(`https://corona.lmao.ninja/v2/historical/${payload}?lastdays=30`)
        .then(res => {
          let data = res.data
          const labels = Object.keys(data.timeline.cases)
          let datasets = {}

          for (const key in data.timeline) {
            datasets[key] = Object.values(data.timeline[key])
          }
          const {cases, deaths, recovered: recoveries} = datasets
          let id = getters.countryId
          const country = new Country(
            id,
            data.country,
            cases[cases.length - 1],
            deaths[deaths.length - 1],
            recoveries[recoveries.length - 1],
            datasets,
            labels
          )
          commit('setLoading', false)
          commit('newCountry', country)
        })
        .catch(err => {
          commit('setLoading', false)
          commit('setError', err.message)
          throw err
        })
    },
    newCountryName: ({commit}, payload) => { commit('newCountryName', payload) },
    updateTopCountries ({commit, dispatch}) {
      commit('clearError')
      commit('setLoading', true)
      axios
        .get('https://corona.lmao.ninja/v2/countries?sort=active')
        .then((res) => {
          let data = res.data.slice(0, 10)
          data = data.map((country) => country.country)
          commit('newTopCountry', data)
          data.forEach(country => {
            dispatch('newCountry', country)
          })
          commit('setLoading', false)
        })
    }
  },
  getters: {
    country: (s) => {
      console.log('new data is', s.countries.find(country => country.name === s.currCountryName))
      return s.countries.find(country => country.name === s.currCountryName) || null
    },
    countryId: (s) => s.countries.length,
    currentCountry: (s) => s.currCountryName,
    topCountries: (s) => s.topCountries
  }
}
