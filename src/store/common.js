export default {
  state: {
    loading: false,
    error: null
  },
  mutations: {
    setLoading: (s, payload) => { s.loading = payload },
    setError: (s, payload) => { s.error = payload },
    clearError: (s) => { s.error = null }
  },
  actions: {
    setLoading: ({commit}, payload) => { commit('setLoading', payload) },
    setError: ({commit}, payload) => { commit('setError', payload) },
    clearError: ({commit}) => { commit('clearError') }
  },
  getters: {
    loading: (s) => s.loading,
    error: (s) => s.err
  }
}
